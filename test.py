# -*- encoding: UTF-8 -*-
import dash
from dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd


sheet = pd.read_excel('Задание аналитик.xlsx', index_col=0)
sheet4 = sheet.pivot_table(index=sheet.index, columns=sheet.columns[0], values=sheet.columns[-1])
sheet1 = sheet.pivot_table(index=sheet.index, columns=sheet.columns[0], values=sheet.columns[1])
shops_code = list(sheet1.index.sort_values(ascending=True))
dates = list(sheet1.columns)
df = sheet1.reset_index()

def pvt_tab():
    a = list((sheet[u'ТО'] / sheet[u'План ТО']) * 100)
    b = list(sheet[u'ТО']-sheet[u'План ТО'])
    for i in range(len(a)):
           if a[i] > 97. : a[i] = '+'
           else: a[i] = b[i]
    sheet['5'] = a
    return sheet.pivot_table(index=sheet.index, columns=sheet.columns[0], values=sheet.columns[-1], aggfunc='first')

def load_shops_code():
    shops_code_options = (
        [{'label': code, 'value': code}
         for code in shops_code]
    )
    return shops_code_options

def load_date():
    date_options = (
        [{'label': date, 'value': date}
         for date in dates]
    )
    return date_options

def generate_table(dataframe, max_rows=20):
    return html.Table(
        # Header
        [html.Tr([html.Th(col) for col in dataframe.columns])] +

        # Body
        [html.Tr([
            html.Td(dataframe.iloc[i][col]) for col in dataframe.columns
        ]) for i in range(min(len(dataframe), max_rows))]
    )


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(children=[
    html.Label(u'Выбор задания'),
    dcc.RadioItems(id='task',
        options=[
            {'label': u'Задание 1', 'value': '1'},
            {'label': u'Задание 2', 'value': '2'},
            {'label': u'Задание 3', 'value': '3'},
            {'label': u'Задание 4', 'value': '4'},
            {'label': u'Задание 5', 'value': '5'},
                ],
         value='1',
         labelStyle={'display': 'inline-block'}
    ),
    html.Label(' '),
    html.Label(u'Код магазина'),
    dcc.Dropdown(id='shops_code',
        options=load_shops_code(),
        multi=True
    ),
    html.Label(u'Дата'),
    dcc.Dropdown(id='date',
        options=load_date(),
        multi=True
    ),
    html.H4(children=u'Сводная таблица'),
    html.Div([
         html.Div(
            html.Table(id='table')
        )
    ])
])
@app.callback(
    Output(component_id='table', component_property='children'),
    [
      Input(component_id='shops_code', component_property='value'),
      Input(component_id='date', component_property='value'),
      Input(component_id='task', component_property='value')
    ]
)
def load_shops_results(code, dat, task):
    tab = sheet1
    if task == '4':tab = sheet4
    if task == '5': tab = pvt_tab()
    if code == None or code == []: code = shops_code
    shop = map(int,list(code))
    if dat == None or dat == []: dat = tab.columns[0:]
    dat = map(pd.Timestamp,dat)
    res = tab.loc[shop,dat]
    res = res.reset_index()
    return generate_table(res, max_rows=20)

if __name__ == '__main__':
    app.run_server()
